class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }
    get age(){
        return this._age;
    }
    get salary(){
        return this._salary;
    }
    set age(age){
        this._age = age;
    }
    set salary(salary){
        this._salary = salary;
    }   
}
class Programmer extends Employee{
    constructor(name, age, salary, lang=[]){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary*3;
    }
    set salary(salary){
        this._salary = salary;
    }
}

e1 = new Employee('Vasya', 20, 17000);
p1 = new Programmer('Andrey', 25, 37000, ['UA', 'EN', 'ru']);
console.log(e1.name, e1.age, e1.salary);
console.log(p1.name, p1.age, p1.salary, p1.lang);